<?php
/*
Author: Javed Ur Rehman
Website: https://htmlcssphptutorial.wordpress.com/
*/
?>
<?php 
require('db.php');
include("auth.php"); //include auth.php file on all secure pages
$uploadstatus = "";
if(isset($_REQUEST['filetitle']))
{
$trn_date = date("Y-m-d H:i:s");
$filetitle =$_REQUEST['filetitle'];
$target_dir = "upload/"; // Where the file is going to be placed
$path = pathinfo($_FILES['upload_file']['name']);
$filename = $path['filename'];
$ext = $path['extension'];
$temp_name = $_FILES['upload_file']['tmp_name'];
$pathfilenameext= $target_dir.$filename.".".$ext;

// Check if file already exists
if (file_exists($pathfilenameext)) {
    $uploadstatus = "Sorry, file already exists.";
	}
else 
	{
	move_uploaded_file($temp_name,$pathfilenameext);
	$uploadstatus = "File Uploaded Successfully.";
	$ins_query="insert into filetable(`trn_date`,`filetitle`,`filename`)values('$trn_date', '$filetitle', '$filename')";
	mysql_query($ins_query) or die(mysql_error());
	}
}
?>
<html>
<head>
<title>Upload File Using PHP and Save in Folder</title>
<link rel="stylesheet" href="css/style.css" />
 <link rel="icon" href="http://orig03.deviantart.net/9b11/f/2008/101/c/5/war_skull_16x16__by_xicidal.gif" type="image/gif" sizes="16x16">
</head>
<body>
<div class="form">
<h1><a href="dashboard.php">Dashboard</a>|<a href="viewfiles.php">View Files</a>|<a href="logout.php">Logout</a></h1>
<h1>Upload File</h1>
<form name="form" method="post" action="" enctype="multipart/form-data" >
<input type="text" name="filetitle" placeholder="Enter File Title:" required /><br /><br />
<input type="file" name="upload_file" id="upload_file" /><br /><br />
<h2><p id="error" style="display:none; color:#c50d9a;">File format should be PDF.</p></h2>
<input type="submit" name="submit" value="Upload" />
</form>
<h2><p style="color:#ff0a0a"><?php echo $uploadstatus; ?></p><h2> 
</div>

<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script>
$('input[type="submit"]').prop("disabled", true);
$('#upload_file').bind('change', function() {
var ext = $('#upload_file').val().split('.').pop().toLowerCase();
if($.inArray(ext, ['pdf','PDF']) == -1)
{ $('#error').slideDown("slow"); } else
{$('#error').slideUp("slow"); $('input:submit').attr('disabled',false);}
});
</script>
</body>
</html>