<?php
/*
Author: Javed Ur Rehman
Website: https://htmlcssphptutorial.wordpress.com/
*/
?>
<?php include("auth.php"); //include auth.php file on all secure pages ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Welcome Home</title>
<link rel="stylesheet" href="css/style.css" />
 <link rel="icon" href="http://orig03.deviantart.net/9b11/f/2008/101/c/5/war_skull_16x16__by_xicidal.gif" type="image/gif" sizes="16x16">
</head>
<body>
<div class="form">
<h1>Welcome <?php echo $_SESSION['username']; ?>!</h1>
<h2><a href="dashboard.php">Dashboard</a></h2>
<h2><a href="logout.php">Logout</a></h2>
</div>
</body>
</html>
