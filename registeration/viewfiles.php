<?php
/*
Author: Javed Ur Rehman
Website: https://htmlcssphptutorial.wordpress.com/
*/
?>
<?php 
require('db.php');
include("auth.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>View Uploaded PDF Files</title>
<link rel="stylesheet" href="css/style.css" />
 <link rel="icon" href="http://orig03.deviantart.net/9b11/f/2008/101/c/5/war_skull_16x16__by_xicidal.gif" type="image/gif" sizes="16x16">
</head>
<body>
<div class="form">
<h2><a href="dashboard.php">Dashboard</a> | <a href="upload.php">Upload New File</a> | <a href="logout.php">Logout</a></h2>
<h2>View Uploaded PDF Files</h2>
<table width="100%" border="1" style="border-collapse:collapse;">
<thead>
<tr><th><strong>S.No</strong></th><th><strong>File Title</strong></th><th><strong>Delete</strong></th></tr>
</thead>
<tbody>
<?php
$count=1;
$sel_query="Select * from filetable;";
$result = mysql_query($sel_query);
while($row = mysql_fetch_assoc($result)) { ?>
<tr><td align="center"><?php echo $count; ?></td><td align="center"><a href="upload/viewpdf.php?file=<?php echo $row["filename"]; ?>.pdf" target="_new"><?php echo $row["filetitle"]; ?></a></td><td align="center"><a href="deletefile.php?id=<?php echo $row["id"]; ?>&filename=<?php echo $row["filename"]; ?>" onclick="return confirm('Are you sure?')">Delete</a></td></tr>
<?php $count++; } ?>
</tbody>
</table>
</div>
</body>
</html>
