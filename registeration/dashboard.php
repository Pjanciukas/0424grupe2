<?php
/*
Author: Javed Ur Rehman
Website: https://htmlcssphptutorial.wordpress.com/
*/
?>
<?php 
require('db.php');
include("auth.php"); //include auth.php file on all secure pages ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Dashboard - View Records</title>
<link rel="stylesheet" href="css/style.css" />
 <link rel="icon" href="http://orig03.deviantart.net/9b11/f/2008/101/c/5/war_skull_16x16__by_xicidal.gif" type="image/gif" sizes="16x16">
</head>
<body>
<div class="form">
<h1>Welcome to Dashboard.</h1>
<h2><a href="index.php">Home</a><h2>
<h2><a href="insert.php">Insert New Record</a></h2>
<h2><a href="view.php">View Records</a><h2>
<h2><a href="upload.php">Upload File</a><h2>
<h2><a href="viewfiles.php">View Files</a><h2>
<h2><a href="logout.php">Logout</a></h2>
</div>
</body>
</html>
